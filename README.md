# About

Creates an interactable "Scissor-Plane", a Plane that is created by a Quadrilateral object, translated, scaled and rotated to create a grid.
