import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Polygon
from matplotlib import collections  as mc
from collections import deque
from matplotlib.artist import Artist

class PolygonInteractor(object):
    showverts = True
    epsilon = 7  # max pixel distance to count as a vertex hit

    def __init__(self, ax, poly, visible=False):
        if poly.figure is None:
            raise RuntimeError('You must first add the polygon to a figure '
                               'or canvas before defining the interactor')
        self.ax = ax
        canvas = poly.figure.canvas
        self.poly = poly
        self.poly.set_visible(visible)

        x, y = zip(*self.poly.xy)
        self.line = Line2D(x, y, ls="", marker='o', markerfacecolor='r', animated=True)
        self.ax.add_line(self.line)

        self.cid = self.poly.add_callback(self.poly_changed)
        self._ind = None  # the active vert

        canvas.mpl_connect('draw_event', self.draw_callback)
        canvas.mpl_connect('button_press_event', self.button_press_callback)
        canvas.mpl_connect('button_release_event', self.button_release_callback)
        canvas.mpl_connect('motion_notify_event', self.motion_notify_callback)
        self.canvas = canvas

    def draw_callback(self, event):
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.poly)
        self.ax.draw_artist(self.line)
        # do not need to blit here, this will fire before the screen is updated

    def poly_changed(self, poly):
        'this method is called whenever the polygon object is called'
        # only copy the artist props to the line (except visibility)
        vis = self.line.get_visible()
        Artist.update_from(self.line, poly)
        self.line.set_visible(vis)  # don't use the poly visibility state


    def get_ind_under_point(self, event):
        'get the index of the vertex under point if within epsilon tolerance'

        # display coords
        xy = np.asarray(self.poly.xy)
        xyt = self.poly.get_transform().transform(xy)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.hypot(xt - event.x, yt - event.y)
        indseq, = np.nonzero(d == d.min())
        ind = indseq[0]

        if d[ind] >= self.epsilon:
            ind = None

        return ind

    def button_press_callback(self, event):
        'whenever a mouse button is pressed'
        if not self.showverts:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        self._ind = self.get_ind_under_point(event)

    def button_release_callback(self, event):
        'whenever a mouse button is released'
        if not self.showverts:
            return
        if event.button != 1:
            return
        self._ind = None

        self.canvas.draw_idle()

    def motion_notify_callback(self, event):
        'on mouse movement'
        if not self.showverts:
            return
        if self._ind is None:
            return
        if event.inaxes is None:
            return
        if event.button != 1:
            return
        x, y = event.xdata, event.ydata
        self.poly.xy[self._ind] = x, y

        self.draw_update()
        update(1)
        self.canvas.draw_idle()

    def draw_update(self):
        self.line.set_data(zip(*self.poly.xy))

        self.canvas.restore_region(self.background)
        self.ax.draw_artist(self.poly)
        self.ax.draw_artist(self.line)
        self.canvas.blit(self.ax.bbox)

def update(val):
    global lines
    lines = []
    x, y = poly.xy[:].T
    Quadrilateral = np.zeros((4, 2))
    Quadrilateral[:,0], Quadrilateral[:,1] = x, y
    Grid_Points = create_Points(Quadrilateral, Grid_Size)
    #Line2.set_data(Grid_Points[:,0], Grid_Points[:,1])
    for i in range(0,Grid_Points.shape[0],4):
        lines.append([(Grid_Points[i,0], Grid_Points[i,1]),(Grid_Points[i+1,0], Grid_Points[i+1,1])])
        lines.append([(Grid_Points[i,0], Grid_Points[i,1]),(Grid_Points[i+3,0], Grid_Points[i+3,1])])
    lc.set_segments(lines)
    Points.set_offsets(Grid_Points)

def create_Points(Original_Quadrilateral: np.ndarray, Shape: tuple):
    Grid = np.zeros(Shape, dtype=bool)

    To_do = deque()
    To_do.append([Original_Quadrilateral, [int(Shape[0]/2),int(Shape[1]/2)]])
    Done = Original_Quadrilateral

    def in_reg(Quad_to_Test: np.ndarray):
        '''
        Test if Quad is completely out of Bounds, and therefor doesn't need to be rendered
        '''
        x_lim = ax1.get_xlim()
        y_lim = ax1.get_ylim()
        if not (x_lim[0] < Quad_to_Test[0,0]) or not (Quad_to_Test[2,0] < x_lim[1]):
            return False
        if not (y_lim[0] < Quad_to_Test[2,1]) or not (Quad_to_Test[0,1] < y_lim[1]):
            return False
        
        #print(Quad_to_Test, "\n", x_lim, y_lim)
        return True

    while len(To_do) != 0:
        [Quad, Pos] = To_do.popleft()
        for Translation in Sides:
            Temp_Pos = [Pos[0] + Translation[0], Pos[1] + Translation[1]]
            if Grid_Size[0]>Temp_Pos[0]>0 and Grid_Size[1]>Temp_Pos[1]>0 and not Grid[Temp_Pos[0],Temp_Pos[1]] and in_reg(Quad):
                New = add_Quad(Quad, Translation)
                To_do.append([New, Temp_Pos])
                Grid[Temp_Pos[0],Temp_Pos[1]] = True
                Done = np.vstack((Done,New))
    #print(Done)
    return Done

def add_Quad(Quad: np.ndarray, Translation: list):
    '''
    Add Quad to existing Quad on given Side
    '''
    if Translation == [0,1]:
        Side_1 = (Quad[3]-Quad[2]) # Vector of the Side from which we work
        Side_2 = -(Quad[1]-Quad[0]) # Vector of the Side to which we put Side_2
        a = 0
        b = 3
    elif Translation == [-1,0]:
        Side_1 = (Quad[0]-Quad[3]) # Vector of the Side from which we work
        Side_2 = -(Quad[2]-Quad[1]) # Vector of the Side to which we put Side_2
        a = 1
        b = 0
    elif Translation == [0,-1]:
        Side_2 = (Quad[3]-Quad[2]) # Vector of the Side from which we work
        Side_1 = -(Quad[1]-Quad[0]) # Vector of the Side to which we put Side_2
        a = 2
        b = 1
    else:
        Side_2 = (Quad[0]-Quad[3]) # Vector of the Side from which we work
        Side_1 = -(Quad[2]-Quad[1]) # Vector of the Side to which we put Side_2
        a = 3
        b = 2
    help1 = np.linalg.inv(np.array([Side_1,[Side_1[1],-Side_1[0]]]))
    help2 = np.array([Side_2,[Side_2[1],-Side_2[0]]])
    New_Quad = (help2@help1@Quad.T).T
    Offset = np.vstack((Quad[a]-New_Quad[b],Quad[a]-New_Quad[b],Quad[a]-New_Quad[b],Quad[a]-New_Quad[b]))
    return New_Quad + Offset

fig, ax1 = plt.subplots()
ax1.set_xlim([-5,5])
ax1.set_ylim([-5,5])

List_quadrilateral = [[1,1],[-1,1],[-1,-1],[1,-1]]
Sides = [[0,1],[-1,0],[0,-1],[1,0]]
Quadrilateral = np.array(List_quadrilateral)

Grid_Size = (20,20)

Grid_Points = create_Points(Quadrilateral, Grid_Size)
lines = []
lc = mc.LineCollection(lines, linewidths=1, color = 'red')
ax1.add_collection(lc)

poly = Polygon(list(zip(Quadrilateral[:,0], Quadrilateral[:,1])), animated=True, closed = False)
ax1.add_patch(poly)
Point = PolygonInteractor(ax1, poly, visible=False)

Points = ax1.scatter(Grid_Points[:,0], Grid_Points[:,1], marker='o', color="grey", s=1)

plt.show()